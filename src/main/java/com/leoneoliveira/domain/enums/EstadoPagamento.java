package com.leoneoliveira.domain.enums;

public enum EstadoPagamento {
	
  PENDENTE("P"),
  QUITADO("Q"),
  CANCELADO("C");
	
	private EstadoPagamento(String descricao) {
		this.descricao = descricao;
	}

	private String descricao;

	public String getDescricao() {
		return descricao;
	}
	
	public static EstadoPagamento toEnum(String descricao) {
		if (descricao == null) {
			return null;
		}
		
		for (EstadoPagamento x : EstadoPagamento.values()) {
			if (descricao.equals(x.getDescricao())) {
				return x;
			}
		}
		throw new IllegalArgumentException("Não foi possivel Localizar a descrição");
	}
}

