package com.leoneoliveira.domain.enums;

public enum TipoCliente {

	PESSOA_FISICA("PF"), 
	PESSOA_JURIDICA("PJ");

	private TipoCliente(String descricao) {
		this.descricao = descricao;
	}

	private String descricao;

	public String getDescricao() {
		return descricao;
	}
	
	public static TipoCliente toEnum(String descricao) {
		if (descricao == null) {
			return null;
		}
		
		for (TipoCliente x : TipoCliente.values()) {
			if (descricao.equals(x.getDescricao())) {
				return x;
			}
		}
		throw new IllegalArgumentException("Não foi possivel Localizar a descrição");
	}
}
