package com.leoneoliveira.domain;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.leoneoliveira.domain.enums.EstadoPagamento;

@Entity
@JsonTypeName("pagamentoComCartao")
public class PagamentoComCartao extends Pagamento {

	private static final long serialVersionUID = 1L;
	private Integer nmParcelas;

	public PagamentoComCartao() {

	}

	public PagamentoComCartao(Integer id, EstadoPagamento estadoPagamento, Pedido pedido, Integer nmParcelas) {
		super(id, estadoPagamento, pedido);
		this.setNmParcelas(nmParcelas);
	}

	public Integer getNmParcelas() {
		return nmParcelas;
	}

	public void setNmParcelas(Integer nmParcelas) {
		this.nmParcelas = nmParcelas;
	}
	
	
}
